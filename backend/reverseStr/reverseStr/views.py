# reversestr/views.py
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Str
from .serializers import StrSerializers

@api_view(['GET'])
def get_str(request):
    # Retrieve the last entered object
    last_str = Str.objects.last()

    # Check if any object exists before proceeding
    if last_str:
        serializer = StrSerializers(last_str)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        return Response({'message': 'No objects found'}, status=status.HTTP_404_NOT_FOUND)

@api_view(['POST'])
def reverse_str(request):
    ori_str = request.data.get('str')
    
    if ori_str:
        rev_str = ori_str[::-1]
        db_update = Str(originalStr=ori_str, reversedStr=rev_str)
        db_update.save()
        return Response({'message': 'String reversed and saved'}, status=status.HTTP_201_CREATED)
    
    return Response({'error': 'No string provided'}, status=status.HTTP_400_BAD_REQUEST)
