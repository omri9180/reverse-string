from rest_framework import serializers
from reverseStr.models import Str


class StrSerializers(serializers.ModelSerializer):
    class Meta:
        model = Str
        fields = '__all__'