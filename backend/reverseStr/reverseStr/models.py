from django.db import models

class Str(models.Model):
    originalStr = models.CharField(max_length=255)
    reversedStr = models.CharField(max_length=255)