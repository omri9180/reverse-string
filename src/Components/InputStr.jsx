import { useEffect, useState } from "react"
import './styles/inputStr.css'
import axios from 'axios';

const InputStr =()=>{
    const[originalStr, setOriginalStr] = useState('')
    const[originalShow,setOriginalShow] = useState('')
    const[reverseStr,setRevesreStr] = useState('')

    const addStrToDB = async ()=>{
        console.log(originalStr)
      await axios.post('http://127.0.0.1:8000/api/reverse_str',{ str: originalStr })
      .then(response =>{
        console.log('Item added with ID:', response.data.id)
      })
      .catch(error => console.error('Error adding to DB:', error))  
    }
    
    const getStrFromDB = async () =>{
      try{
        const response = await axios.get('http://127.0.0.1:8000/api/get_str')
        setOriginalShow(response.data.originalStr)
        setRevesreStr(response.data.reversedStr)
      
      }catch(error){
        console.error('Error',error)
    }
  }
  const handleSubmit = async (e) => {
    e.preventDefault();
    await addStrToDB();
    await getStrFromDB();
};


    return(
        <>
        <form onSubmit={handleSubmit} id="input_form">
            <input onChange={(e)=>{setOriginalStr(e.target.value)}} 
            value={originalStr} 
            type="text" 
            name="str" 
            placeholder="insert String to reverse">
            </input>
            <button type="submit">resverse string</button>
        </form>
        <label>original String: </label>
        <h4>{originalShow}</h4>
        <label>reverse String: </label>
        <h3>{reverseStr}</h3>
        </>
    )
}

export default  InputStr